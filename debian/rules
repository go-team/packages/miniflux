#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

# Include all assets, templates, etc. in build directory
export DH_GOLANG_INSTALL_ALL := 1

GOPATH = $(CURDIR)/_build
GO_IMPORT_PATH = miniflux.app/v2

export DH_GOLANG_BUILDPKG := $(GO_IMPORT_PATH)

# Use changelog date instead of actual current date to ensure reproducible build
BUILD_DATE   := $(shell date +%FT%T%z -d @$(SOURCE_DATE_EPOCH))
LD_FLAGS := -s -w \
	-X 'miniflux.app/v2/internal/version.Version=$(DEB_VERSION_UPSTREAM)' \
	-X 'miniflux.app/v2/internal/version.BuildDate=$(BUILD_DATE)'

%:
	dh $@ --builddirectory=_build --buildsystem=golang --with=golang --with=installsysusers

override_dh_auto_build:
	dh_auto_build -- -ldflags "$(LD_FLAGS)"
	# Rename binary to miniflux
	mv $(GOPATH)/bin/v2 $(GOPATH)/bin/miniflux

override_dh_auto_install:
	dh_auto_install -- --no-source

override_dh_installchangelogs:
	dh_installchangelogs ChangeLog
