Source: miniflux
Section: web
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Maytham Alsudany <maytha8thedev@gmail.com>,
           James Valleroy <jvalleroy@mailbox.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-exec,
 dh-golang,
 po-debconf,
 golang-any,
 golang-github-abadojack-whatlanggo-dev,
 golang-github-andybalholm-brotli-dev (>= 1.1.0),
 golang-github-coreos-go-oidc-v3-dev,
 golang-github-go-webauthn-webauthn-dev,
 golang-github-google-uuid-dev,
 golang-github-gorilla-mux-dev,
 golang-github-lib-pq-dev,
 golang-github-prometheus-client-golang-dev,
 golang-github-puerkitobio-goquery-dev,
 golang-github-tdewolff-minify-dev,
 golang-github-yuin-goldmark-dev,
 golang-golang-x-crypto-dev,
 golang-golang-x-image-dev,
 golang-golang-x-net-dev,
 golang-golang-x-oauth2-dev,
 golang-golang-x-term-dev,
 golang-mvdan-xurls-dev,
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/miniflux
Vcs-Git: https://salsa.debian.org/go-team/packages/miniflux.git
Homepage: https://miniflux.app
XS-Go-Import-Path: miniflux.app/v2

Package: miniflux
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 adduser (>= 3.11),
 debconf,
 dbconfig-pgsql | dbconfig-no-thanks,
 ucf,
Built-Using: ${misc:Built-Using}
Static-Built-Using: ${misc:Static-Built-Using}
Description: minimalist and opinionated feed reader
 Miniflux is a minimalist and opinionated feed reader:
   - Written in Go (Golang)
   - Works only with PostgreSQL
   - Doesn't use any ORM
   - Doesn't use any complicated framework
   - Use only modern vanilla Javascript (ES6 and Fetch API)
   - Single binary compiled statically without dependency
   - The number of features is voluntarily limited
 It's simple, fast, lightweight and super easy to install.
